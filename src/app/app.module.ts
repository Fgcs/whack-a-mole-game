import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MolesModule } from './moles/moles.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MolesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
