import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoleComponent } from './components/mole/mole.component';
import { HomeComponent } from './components/home/home.component';
import { StartGameComponent } from './components/start-game/start-game.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    MoleComponent,
    HomeComponent,
    StartGameComponent
  ],
  imports: [
    CommonModule, ReactiveFormsModule
  ],
  exports: [
    HomeComponent
  ],
})
export class MolesModule { }
