import { Component, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-start-game',
  templateUrl: './start-game.component.html',
  styleUrls: ['./start-game.component.css']
})
export class StartGameComponent {
  inputQuantity: FormControl = new FormControl(6);
  @Output() startGame = new EventEmitter<number>();


  onStartGame() {


    this.startGame.emit(this.inputQuantity.value);
  }
}
