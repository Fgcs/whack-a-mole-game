import { AfterViewInit, Component, OnChanges, QueryList, SimpleChanges, ViewChildren } from '@angular/core';
import { MoleComponent } from '../mole/mole.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  @ViewChildren(MoleComponent) moles!: QueryList<MoleComponent>;
  totalQuantity: any[] = [];
  showGame: boolean = false;
  lastHole?: MoleComponent;
  timeUp: boolean = false;
  status: boolean = true;
  score: number = 0;
  startGame(quantity: number) {
    if (this.status) {

      this.score = 0;
      this.status = false;
      for (let index = 0; index < quantity; index++) {
        this.totalQuantity.push(index);
      }
      this.showGame = true;
      this.moles.changes.subscribe((r) => {
        this.moles = r;
        this.timeUp = false;
        this.peep();
      });
      setTimeout(() => {
        this.timeUp = true;
        this.status = true;
        this.totalQuantity = [];
        this.showGame = false;
      }, 10000);
    }
  }
  randomTime(min: number, max: number) {
    return Math.round(Math.random() * (max - min) + min);
  }
  randomHole(): MoleComponent {
    const index = Math.floor(Math.random() * this.moles.length);
    const hole = this.moles.get(index);
    if (hole === this.lastHole) {
      return this.randomHole();
    }
    this.lastHole = hole;
    return hole as MoleComponent;
  }
  peep() {
    const time = this.randomTime(200, 1000);
    const hole = this.randomHole();

    hole?.moleUp();
    setTimeout(() => {
      hole?.moleDown();
      if (!this.timeUp) this.peep();
    }, time);
  }
  onBonk() {
    this.score++;
  }
}
