import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, Output } from '@angular/core';

@Component({
  selector: 'app-mole',
  templateUrl: './mole.component.html',
  styleUrls: ['./mole.component.css']
})
export class MoleComponent {
  @Input() id: number = 0;
  @Output() bonk = new EventEmitter<boolean>();
  constructor(@Inject(DOCUMENT) private document: Document) { }

  moleUp() {
    const mole = this.document.getElementById(`${this.id}`);
    mole?.classList.toggle('up');
  }
  moleDown() {
    const mole = this.document.getElementById(`${this.id}`);
    mole?.classList.remove('up');
  }
  onBonk() {

    this.bonk.emit(true);
    this.moleDown();
  }
}
